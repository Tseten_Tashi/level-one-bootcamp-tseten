//WAP to find the sum of n fractions.
#include<stdio.h>

typedef struct fractions
{
	int num, den;
}Fractions;

int get_num()
{
	int temp;
	printf("Please Enter The No. Of  Fraction You Want To Put: \n");
	scanf("%d",&temp);
	if( temp < 0)
    {
        printf("Please enter a valid positive no: \n");
        return get_num();
    }
    return temp;
}

void get_input(Fractions *a, int no )
{
	for(int i=0;i<no;i++)
	{
		printf("Please enter the the numerator:\n");
		scanf("%d",&a[i].num);
		printf("Please enter the denominator: \n");
		scanf("%d",&a[i].den);
	}
}

void show_output(Fractions temp, Fractions *a, int no)
{
	printf("The sum of the fractions  ");
	for(int i=0;i<no;i++)
	{
		if(i==no-1)
		{
			printf("%d/%d = %d/%d", a[i].num, a[i].den,temp.num,temp.den);
		}
		else
		{
			printf("%d/%d +",a[i].num,a[i].den);
		}
	}
}

int gcd(int x,int y)
{
	if(x==0)
	{
		return y;
	}
	return gcd(y%x,x);
}


Fractions calculate(int no, Fractions *a)
{
	Fractions final;
	int x, y=0, test=1;
	for(int i=0; i<no;i++)
	{
		test =test*a[i].den;
	}
	final.den=test;

	for(int i=0;i<no;i++)
	{
		x=a[i].num;
		for(int j=0;j<no;j++)
		{
			if(i!=j)
			{
				x=x*a[j].den;
			}
		}
		y=y+x;
	}
	final.num=y/gcd(y,test);
	final.den=test/gcd(y,test);
	return final;
}

int main()
{
    int no;
    Fractions final;
    no=get_num();
    Fractions a[no];
    get_input(a,no);
    final=calculate(no,a);

    show_output(final,a,no);
    return 0;
}
