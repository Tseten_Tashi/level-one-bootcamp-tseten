//Write a program to add two user input numbers using 4 functions.

#include <stdio.h>
int input();
int add(int a, int b);
void output(int s);

void main(){
int a,b,s;
a=input();
b=input();
s=add(a,b);
output(s);
}

int input(){
int a;
printf("Enter the no:\n");
scanf("%d",&a);
return a;
}
int add(int a, int b){
return a +b;
}
void output(int s){
printf("The sum is: %d\n", s);
}