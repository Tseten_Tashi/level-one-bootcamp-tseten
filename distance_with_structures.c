//WAP to find the distance between two points using structures and 4 functions.

#include<stdio.h>
#include<math.h>

typedef struct pt
{
float x,y;
}pt;

pt input(pt v)
{
printf("\nEnter for X: ");
scanf("%f",&v.x);
printf("\nEnter for Y: ");
scanf("%f",&v.y);
return v;
}

float calc(pt a, pt b)
{
float t;
t=sqrt(pow(a.x-b.x,2)+pow(a.y-b.y,2));
return t;
}

void output(float dis)
{
printf("\nThe distance b/w the points is: %2f",dis);
}

int main()
{
pt a,b;
float d;
printf("\nEnter the coordinates for first point");
a=input(a);
printf("\nEnter the coordinate for second point");
b=input(b);
d=calc(a,b);
output(d);
return 0;
}
