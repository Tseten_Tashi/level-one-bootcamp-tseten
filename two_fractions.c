//WAP to find the sum of two fractions.

#include<stdio.h>

typedef struct frac
{
int num,den;
}frac;

frac input()
{
frac v;
printf("Enter the numerator:\n");
scanf("%d",&v.num);
printf("Enter the denominator:\n");
scanf("%d",&v.den);
return v;
}

int gcd(int a,int b)
{
if(a==0)
{
return b;
}
return gcd(b%a,a);
}

frac calc(frac x,frac y)
{
frac z;
int a,b;
a=(x.num*y.den)+(x.den*y.num);
b=(x.den*y.den);
z.num=a/gcd(a,b);
z.den=b/gcd(a,b);
return z;
}

void output(frac o)
{
printf("The sum is: %d/%d",o.num,o.den);
}

int main()
{
frac q,w,e;
printf("Enter the 1st fraction\n");
q=input();
printf("Enter the 2nd fraction\n");
w=input();
e=calc(q,w);
output(e);
}
