//WAP to find the distance between two point using 4 functions.

#include<stdio.h>
#include<math.h>
float input()
{
float v;
scanf("%f",&v);
return v;
}

float calc(float x1, float y1, float x2, float y2)
{
float d;
d=(sqrt(pow(x2-x1,2)+pow(y2-y1,2)));
return d;
}

void output(float dis)
{
printf("The distance is=%f\n",dis);
}

int main()
{
float a1, a2, b1, b2, c;
printf("Enter the coordinate of first point: \n");
printf("Enter the X coordinates\n");
a1=input();
printf("Enter the Y coordinate\n");
b1=input();
printf("Enter the coordinate of second point: \n");
printf("Enter the X coordinates\n");
a2=input();
printf("Enter the Y coordinate\n");
b2=input();
c=calc(a1, b1, a2, b2);
output(c);
}
